package ma.octo.assignement.dto;


import lombok.Data;
import lombok.NoArgsConstructor;
import ma.octo.assignement.domain.Compte;

import java.math.BigDecimal;
import java.util.Date;
@Data
@NoArgsConstructor
public class VersementDto {
    private BigDecimal montantVersement;
    private Date dateExecution;
    private String nrnom_prenom_emetteur;
    private String nrcompteBeneficiaire;
    private String motifVersement;


}
