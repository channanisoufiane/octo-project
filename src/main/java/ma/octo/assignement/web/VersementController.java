package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.*;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AutiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController(value = "/versement")

public class VersementController {

    public static final int MONTANT_MAXIMAL = 10000;

    public static final int MONTANT_MINIMAL = 10;
    Logger LOGGER = LoggerFactory.getLogger(VersementController.class);
    @Autowired
    private CompteRepository rep1;
    @Autowired
    private VersementRepository re2;
    @Autowired
    private AutiService monservice;
    @Autowired
    private UtilisateurRepository re3;

    @GetMapping("lister_versement")
    List<Versement> loadAll() {
        List<Versement> all = re2.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }



    @PostMapping("/executerVersement")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional(rollbackFor = Exception.class)
    public String createTransaction(@RequestBody VersementDto versementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException, UtilisateurNonExistantException, RIBNonExistantException {

        Utilisateur u1 = re3.findByUsername(versementDto.getNrnom_prenom_emetteur());
        System.out.println(versementDto.getNrcompteBeneficiaire());
        Compte f12 = rep1
                .findByRib(versementDto.getNrcompteBeneficiaire());

        if (u1 == null) {
            System.out.println("Utilisateur Non existant");
            throw new UtilisateurNonExistantException("Utilisateur Non existant");
        }

        if (f12 == null) {
            System.out.println("RIB Non existant");
            throw new RIBNonExistantException("RIB Non existant");
        }

        if (versementDto.getMotifVersement().equals("")||versementDto.getMotifVersement()==null) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVersement().intValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVersement().intValue() < MONTANT_MINIMAL) {
            System.out.println("Montant minimal de versement non atteint");
            throw new TransactionException("Montant minimal de versement non atteint");
        } else if (versementDto.getMontantVersement().intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de versement dépassé");
            throw new TransactionException("Montant maximal de versement dépassé");
        }



        f12
                .setSolde(new BigDecimal(f12.getSolde().intValue() + versementDto.getMontantVersement().intValue()));
        if(rep1.save(f12)==null)
            throw new TransactionException("Probleme se produit");

        Versement versement = new Versement();
        versement.setDateExecution(versementDto.getDateExecution());
        versement.setCompteBeneficiaire(f12);
        versement.setMontantVirement(versementDto.getMontantVersement());

        if(re2.save(versement)==null)
            throw new TransactionException("Probleme se produit");

        monservice.auditVersement("Versement depuis " + versementDto.getNrnom_prenom_emetteur() + " vers " + versementDto
                .getNrcompteBeneficiaire() + " d'un montant de " + versementDto.getMontantVersement()
                .toString());
        return "Paiment effectue";
    }
    private void save(Versement Versement) {
        re2.save(Versement);
    }

}
