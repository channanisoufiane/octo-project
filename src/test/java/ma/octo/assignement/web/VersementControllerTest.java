package ma.octo.assignement.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.*;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.*;

public class VersementControllerTest {


    @Test
    void loadAll() {
    }

    @Test
     void createTransactionSuccess()
            throws Exception {
        VersementDto versementDto = new VersementDto();
        versementDto.setMontantVersement(BigDecimal.TEN);
        versementDto.setMotifVersement("sss");
        versementDto.setDateExecution(new Date());
        versementDto.setNrcompteBeneficiaire("RIB1");
        versementDto.setNrnom_prenom_emetteur("user2");

        RestTemplate restTemplate = new RestTemplate();

        final String baseUrl = "http://localhost:8050" +"/executerVersement";
        URI uri = new URI(baseUrl);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ObjectMapper objectMapper = new ObjectMapper();
        HttpEntity<String> request =
                new HttpEntity<String>( objectMapper.writeValueAsString(versementDto), headers);
        ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);

        //Verify request succeed
        Assert.assertEquals(201, result.getStatusCodeValue());
        Assert.assertEquals(true, result.getBody().equals("Paiment effectue"));
        //return res ;
    }

    @Test
    void createTransactionUserNotFount()
            throws Exception {
            try {
                VersementDto versementDto = new VersementDto();
                versementDto.setMontantVersement(BigDecimal.TEN);
                versementDto.setMotifVersement("sss");
                versementDto.setDateExecution(new Date());
                versementDto.setNrcompteBeneficiaire("RIB1");
                versementDto.setNrnom_prenom_emetteur("userss2");

                RestTemplate restTemplate = new RestTemplate();

                final String baseUrl = "http://localhost:8050" + "/executerVersement";
                URI uri = new URI(baseUrl);
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                ObjectMapper objectMapper = new ObjectMapper();
                HttpEntity<String> request =
                        new HttpEntity<String>(objectMapper.writeValueAsString(versementDto), headers);
                ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);

                //Verify request succeed
                Assert.assertEquals(451, result.getStatusCodeValue());
                Assert.assertEquals(true, result.getBody().equals("Utilisateur introuvable"));
            }
            catch (Exception e)
            {
                if(!e.getMessage().contains("Utilisateur introuvable"))
                    Assert.assertEquals(true,false);
            }
            //return res ;
    }
}